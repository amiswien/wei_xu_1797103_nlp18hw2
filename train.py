import tensorflow as tf
import numpy as np
from preprocessing import *

train_path = 'semcor.data.xml'

# set parameters
embedding_size = 300
learning_rate = 0.001
hidden_size = 512
batch_size = 32
num_of_epochs = 1
keep_prop = 0.5

# parse the JSON file to get vocabulary, dictionary for words and senses and build training dataset
data, vocabulary, vocab_size, dictionary, reverse_dictionary = read_data(train_path)
data_sense, vocabulary_sense, vocab_size_sense, dictionary_sense, reverse_dictionary_sense = read_data_sense(train_path)

# generate specfic word embeddings based on training data
embeddings= word2vec(vocabulary, embedding_size)

graph = tf.Graph()

with graph.as_default():

    with tf.name_scope('inputs'):
        # shape = (batch size, max length of sentence in batch)
        word_ids = tf.placeholder(tf.int32, shape=[None, None], name='word_ids')
        # shape = (batch size)
        sequence_lengths = tf.placeholder(tf.int32, shape=[None], name='sequence_lengths')
        # shape = (batch size, max length of sentence in batch)
        labels = tf.placeholder(tf.int32, shape=[None, None], name='labels')

    with tf.variable_scope('embeddings'):
        L = tf.Variable(embeddings, dtype=tf.float32, trainable=True, name='L')
        # shape = (batch, sentence, word_vector_size)
        pretrained_embeddings = tf.nn.embedding_lookup(L, word_ids)

    with tf.variable_scope("bilstm"):
        cell_fw = tf.nn.rnn_cell.DropoutWrapper(tf.nn.rnn_cell.BasicLSTMCell(hidden_size), input_keep_prob=keep_prop)
        #cell_fw = tf.nn.rnn_cell.MultiRNNCell([cell_fw]*2)
        cell_bw = tf.nn.rnn_cell.DropoutWrapper(tf.nn.rnn_cell.BasicLSTMCell(hidden_size), input_keep_prob=keep_prop)
        #cell_bw = tf.nn.rnn_cell.MultiRNNCell([cell_bw]*2)
        (output_fw, output_bw), _ = tf.nn.bidirectional_dynamic_rnn(cell_fw,
            cell_bw, pretrained_embeddings, sequence_length=sequence_lengths,
            dtype=tf.float32)
        # concatenate the forwad and backward output of the bi-LSTM
        context_rep = tf.concat([output_fw, output_bw], axis=-1)

    with tf.variable_scope("output"):
        W = tf.get_variable(name="W",
                            shape=[hidden_size*2, vocab_size_sense],
                            initializer=tf.truncated_normal_initializer(stddev=0.05),
                            dtype=tf.float32)
        b = tf.get_variable(name="b",
                            shape=[vocab_size_sense],
                            initializer=tf.constant_initializer(value=0.),
                            dtype=tf.float32)
        ntime_steps = tf.shape(context_rep)[1]
        # squeeze the first two dimensions into one demension
        context_rep_flat = tf.reshape(context_rep, [-1, 2*hidden_size]) #shape=(batch*step,hidden)
        pred = tf.matmul(context_rep_flat, W) + b
        # reshape the vector to obtain per-timestep tag representation
        scores = tf.reshape(pred, [-1, ntime_steps, vocab_size_sense]) #shape=(batch,step,hidden)
        tf.add_to_collection("scores", scores)

    with tf.name_scope("loss"):
        losses = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=scores, labels=labels)
        # shape = (batch, sentence, nclasses)
        mask = tf.sequence_mask(sequence_lengths)
        # apply mask
        losses = tf.boolean_mask(losses, mask)

        loss = tf.reduce_mean(losses)

    tf.summary.scalar('loss', loss)

    with tf.name_scope("optimizer"):
        # loss and optimizer
        optimizer = tf.train.AdamOptimizer(learning_rate)
        train_op = optimizer.minimize(loss)

    with tf.name_scope("pred"):
        # get the argmax and cast the result to an interger
        lables_pred = tf.cast(tf.argmax(scores, axis=-1), tf.int32)
        correct_pred = tf.equal(lables_pred, labels)
        accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32), name="accuracy")

    merged = tf.summary.merge_all()

    init = tf.global_variables_initializer()

    saver = tf.train.Saver()

with tf.Session(graph=graph) as sess:
    writer = tf.summary.FileWriter('tmp/', sess.graph)
    sess.run(init)

    # split dataset to batch size for epochs
    for epo, epoch in enumerate(generate_epochs(data, data_sense, batch_size, num_of_epochs)):
        acc_total = 0
        loss_total = 0
        # generate batch for each step
        for step, (X, y) in enumerate(epoch):
            # pad the batch to the longest input sentence
            max_length = len(max(X, key=len))
            new_X = np.random.rand(batch_size, max_length)
            new_y = np.random.rand(batch_size, max_length)
            for i in range(len(X)):
                if len(X[i]) == max_length:
                    new_X[i] = np.array(X[i])
                    new_y[i] = np.array(y[i])
                else:
                    new_X[i] = np.pad(X[i], (0, (max_length - len(X[i]))), 'constant', constant_values=0)
                    new_y[i] = np.pad(X[i], (0, (max_length - len(y[i]))), 'constant', constant_values=0)

            _, loss_val, acc = sess.run([train_op, loss, accuracy], feed_dict={word_ids: new_X, sequence_lengths: np.array([len(i) for i in X]), labels: new_y})
            # sum up loss and accuracy for one epoch
            acc_total += acc
            loss_total += loss_val
        # average and print the loss and accuracy
        print("epoch: " + str(epo) + " loss:" + str(loss_total/step))
        print("accuracy: " + str(acc_total/step))

    # save the model and graph
    saver.save(sess, 'tmp/my-model')
    saver.export_meta_graph('tmp/my-model.meta')
    writer.close()
