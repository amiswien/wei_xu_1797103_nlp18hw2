import tensorflow as tf
import numpy as np
from preprocessing import *

train_path = 'semcor.data.xml'
evaluation_path = 'ALL.data.xml'

# set parameters
embedding_size = 300
learning_rate = 0.001
hidden_size = 512
batch_size = 32
num_of_epochs = 1
keep_prop = 0.5

# parse the JSON file to get vocabulary, dictionary for words and senses and build evaluation dataset
data, vocabulary, vocab_size, dictionary, reverse_dictionary = read_data(train_path)
data_eval = build_dataset(evaluation_path, vocabulary, dictionary, 'semeval2015')

data_sense, vocabulary_sense, vocab_size_sense, dictionary_sense, reverse_dictionary_sense = read_data_sense(train_path)
data_sense_eval = build_dataset_sense(evaluation_path, vocabulary_sense, dictionary_sense, 'semeval2015')

with tf.Session() as sess:
    # restore graph and checkpoints
    new_saver = tf.train.import_meta_graph('tmp/my-model.meta')
    new_saver.restore(sess, tf.train.latest_checkpoint('tmp/'))

    # split dataset to batch size for epochs
    for epo, epoch in enumerate(generate_epochs(data_eval, data_sense_eval, batch_size, num_of_epochs)):
        acc_total = 0
        # generate batch for each step
        for step, (X, y) in enumerate(epoch):
            # pad the batch to the longest input sentence
            max_length = len(max(X, key=len))
            new_X = np.random.rand(batch_size, max_length)
            new_y = np.random.rand(batch_size, max_length)
            for i in range(len(X)):
                if len(X[i]) == max_length:
                    new_X[i] = np.array(X[i])
                    new_y[i] = np.array(y[i])
                else:
                    new_X[i] = np.pad(X[i], (0, (max_length - len(X[i]))), 'constant', constant_values=0)
                    new_y[i] = np.pad(X[i], (0, (max_length - len(y[i]))), 'constant', constant_values=0)
            acc = sess.run("pred/accuracy:0", feed_dict={"inputs/word_ids:0": new_X, "inputs/sequence_lengths:0": np.array([len(i) for i in X]), "inputs/labels:0": new_y})
            # sum up accuracy for one epoch
            acc_total += acc
        # average and print the accuracy
        print("accuracy: " + str(acc_total / step))
