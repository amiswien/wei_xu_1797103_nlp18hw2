import numpy as np
import xml.etree.ElementTree as ET
from collections import Counter

def read_data(file):
    tree = ET.parse(file)
    root = tree.getroot()
    words = []
    data = []
    dictionary = {}
    # parse xml file to obtain vocabulary and dataset
    for text in root:
        for sentence in text:
            senten = []
            for token in sentence:
                senten.append(token.text.lower().replace(' ', '-'))
                words.append(token.text.lower().replace(' ', '-'))
            data.append(senten)
    words.append('UNK')
    # obtain dictionary and reverse dictionary
    for word, _ in (Counter(words)).items():
        dictionary[word] = len(dictionary)
    reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
    # convert words in dataset to integers
    for i in range(len(data)):
        for j in range(len(data[i])):
            data[i][j] = dictionary[data[i][j]]
    data.sort(key=len)
    # obtain vocabulary and vocabulary size
    vocabulary = list(dictionary.keys())
    vocab_size = len(vocabulary)

    return data, vocabulary, vocab_size, dictionary, reverse_dictionary

def read_data_sense(file):
    data = []
    senses = []
    bn = {}
    dictionary = {}
    # read the txt file and obtain a dictionary for senses
    with open('semcor.gold.key.bnids.txt') as k:
        for line in k.readlines():
            line = line.rstrip().split()
            bn[line[0]] = line[1][3:]

    tree = ET.parse(file)
    root = tree.getroot()
    # parse xml file and convert tokens to senses
    for text in root:
        for sentence in text:
            senten = []
            for token in sentence:
                if token.tag == 'wf':
                    senten.append(token.text)
                    senses.append(token.text)
                elif token.tag == 'instance':
                    senten.append(bn[token.attrib['id']])
                    senses.append(bn[token.attrib['id']])
            data.append(senten)
    senses.append('UNK')
    # obtain dictionary and reverse dictionary
    for sense, _ in (Counter(senses)).items():
        dictionary[sense] = len(dictionary)
    reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
    # convert senses in dataset to integers
    for i in range(len(data)):
        for j in range(len(data[i])):
            data[i][j] = dictionary[data[i][j]]
    data.sort(key=len)
    # obtain vocabulary and vocabulary size
    vocabulary = list(dictionary.keys())
    vocab_size = len(vocabulary)

    return data, vocabulary, vocab_size, dictionary, reverse_dictionary


def word2vec(vocabulary, embedding_size):

    glove_file = 'glove.42B.300d.txt'
    word2vec = {}
    unfound = []
    # read the glove embedding file and convert it to an embedding dictionary
    with open(glove_file, 'r') as file:
        for line in file.readlines():
            line = line.split()
            vector = np.array(line[1:], dtype = np.float32)
            word2vec[line[0]] = vector
    # create specfic word embeddings for dataset
    embeddings = np.zeros([len(vocabulary), embedding_size], dtype=np.float32)
    # obtain embeddings from glove word embeddings and count the number of words excluded
    num_not_found = 0
    for i in range(len(vocabulary)):
        if vocabulary[i] in word2vec:
            embeddings[i, :] = word2vec[vocabulary[i]]
        else:
            num_not_found += 1
            embeddings[i, :] = np.random.normal(0.0, 0.1, embedding_size)
    print('n words not found in glove word vectors: ' + str(num_not_found))

    return embeddings

def generate_batch(X, y, batch_size):
    for i in range(0, len(X), batch_size):
        yield np.array(X[i:i+batch_size]), np.array(y[i:i+batch_size])

def generate_epochs(X, y, batch_size, num_of_epochs):
    lx = (len(X)//batch_size)*batch_size
    X = X[:lx]
    y = y[:lx]
    for i in range(num_of_epochs):
        yield generate_batch(X, y, batch_size)

def build_dataset(file, vocabulary, dictionary, dataset_name):
    tree = ET.parse(file)
    root = tree.getroot()
    data = []
    # parse xml file to obtain evaluation dataset
    for text in root:
        if text.attrib['id'].startswith(dataset_name):
            for sentence in text:
                senten = []
                for token in sentence:
                    senten.append(token.text.lower().replace('_', '-').replace(' ', '-'))
                data.append(senten)
    # convert words in dataset to integers
    for i in range(len(data)):
        for j in range(len(data[i])):
            if data[i][j] in vocabulary:
                data[i][j] = dictionary[data[i][j]]
            else:
                data[i][j] = dictionary['UNK']
    data.sort(key=len)

    return data

def build_dataset_sense(file, vocabulary_sense, dictionary_sense, dataset_name):
    data = []
    bn = {}
    # read the txt file and obtain a dictionary for senses
    with open('ALL.gold.key.bnids.txt') as k:
        for line in k.readlines():
            if line.startswith(dataset_name):
                line = line.rstrip().split()
                bn[line[0]] = line[1][3:]

    tree = ET.parse(file)
    root = tree.getroot()
    # parse xml file and convert tokens to senses
    for text in root:
        if text.attrib['id'].startswith(dataset_name):
            for sentence in text:
                senten = []
                for token in sentence:
                    if token.tag == 'wf':
                        senten.append(token.text)
                    elif token.tag == 'instance':
                        senten.append(bn[token.attrib['id']])
                data.append(senten)
    # convert words in dataset to integers
    for i in range(len(data)):
        for j in range(len(data[i])):
            if data[i][j] in vocabulary_sense:
                data[i][j] = dictionary_sense[data[i][j]]
            else:
                data[i][j] = dictionary_sense['UNK']
    data.sort(key=len)

    return data
