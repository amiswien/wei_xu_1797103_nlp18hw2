import tensorflow as tf
import numpy as np
from preprocessing import *

train_path = 'semcor.data.xml'
# parse the JSON file to get vocabulary, dictionary for words and senses
data, vocabulary, vocab_size, dictionary, reverse_dictionary = read_data(train_path)
data_sense, vocabulary_sense, vocab_size_sense, dictionary_sense, reverse_dictionary_sense = read_data_sense(train_path)
# load test file and preprocess it
data_test = []
with open('test_data.txt', 'r') as f:
    lines = f.read().split()
    start = 0
    for i in range(len(lines)):
        lines[i] = lines[i].split('|')[0]
        if lines[i].startswith('.'):
            data_test.append(lines[start:i + 1])
            start = i + 1
# convert word to integer
for i in range(len(data_test)):
    for j in range(len(data_test[i])):
        if data_test[i][j] in vocabulary:
            data_test[i][j] = dictionary[data_test[i][j]]
        else:
            data_test[i][j] = dictionary['UNK']

with tf.Session() as sess:
    # restore graph and checkpoints
    new_saver = tf.train.import_meta_graph('tmp/my-model.meta')
    new_saver.restore(sess, tf.train.latest_checkpoint('tmp/'))
    # pad the batch to the longest input sentence
    max_length = len(max(data_test, key=len))
    new_X = np.random.rand(len(data), max_length)
    for i in range(len(data_test)):
        if len(data_test[i]) == max_length:
            new_X[i] = np.array(data_test[i])
        else:
            new_X[i] = np.pad(data_test[i], (0, (max_length - len(data_test[i]))), 'constant', constant_values=0)
    # predict result
    scores = sess.run(tf.get_collection("scores")[0], feed_dict={"inputs/word_ids:0": new_X, "inputs/sequence_lengths:0": np.array([len(i) for i in data_test])})
    lables_pred = tf.cast(tf.argmax(scores, axis=-1), tf.int32)
    print(lables_pred)
